#!/usr/bin/python
from BaseHTTPServer import BaseHTTPRequestHandler,HTTPServer
import paramiko
import json

config = json.loads(open('config_monitor.json').read())
PORT_NUMBER = config["options"].get('port', 6415)

import parsers

#This class will handle any incoming request from
#the browser 
class monitorServers(object):
    def __init__(self, config):
        self.config = config
        self.servers = self.config['servers']
        self.columns = self.config['options'].get('columns', 2)
        self.refresh = self.config['options'].get('refresh', 5)
        self.css = open('themes/%s.css' % self.config['options'].get('theme', 'default')).read()
        for server in self.servers.keys():
            self.servers[server]['session']=paramiko.SSHClient()
            self.servers[server]['session'].set_missing_host_key_policy(paramiko.AutoAddPolicy())
            self.servers[server]['session'].connect(self.servers[server]['ip'],username=self.servers[server].get('username', self.servers.get('username', None)),password=self.servers[server].get('password',self.servers.get('password', None)))

    def get_monit(self, server):
        stdin, stdout, stderr = self.servers[server]['session'].exec_command(self.servers[server].get('command',self.servers.get('command', None)))
        self.servers[server]['last_output'] = stdout.read()
    
    def update(self):
        for i in self.servers.keys():
            self.get_monit(i)

class myHandler(BaseHTTPRequestHandler):
    monitor = monitorServers(config)
    #Handler for the GET requests
    def do_GET(self):
        self.send_response(200)
        self.send_header('Content-type','text/html')
        self.end_headers()
        self.wfile.write('<!DOCTYPE html><html><head>')
        self.wfile.write(self.monitor.css)
        self.wfile.write('</head><body>')
        # Send the html message
        self.monitor.update()
        self.wfile.write('<meta http-equiv="refresh" content="%s"/><table id="servers">\n' % self.monitor.refresh)
        idx=0
        for server in self.monitor.servers.keys():
            idx+=1
            self.wfile.write('<tr><td>' if idx%self.monitor.columns == 1 else '</td>\n<td>')
            self.wfile.write('<b>%s</b> (%s)' % (server, self.monitor.servers[server]['ip']))
            try:
                p = parsers.PARSERS[self.monitor.servers[server]['parser']]
                self.wfile.write(p(self.monitor.servers[server]['last_output']))
            except Exception, e:
                print e
                print 'Unable to load specific parser, defaulting to plain text mode'
                self.wfile.write('</br><table id="services">')
                for line in self.monitor.servers[server]['last_output'].splitlines():
                    self.wfile.write('<tr><td>%s</td></tr>' % line)
                self.wfile.write('</table>')
            self.wfile.write('</td></tr>' if idx%self.monitor.columns == 0 else '</td>')
        self.wfile.write('</table></body></html>')
        return

try:
    #Create a web server and define the handler to manage the
    #incoming request
    server = HTTPServer(('', PORT_NUMBER), myHandler)
    print 'Started httpserver on port ' , PORT_NUMBER    
    #Wait forever for incoming htto requests
    server.serve_forever()

except KeyboardInterrupt:
    print '^C received, shutting down the web server'
    server.socket.close()
