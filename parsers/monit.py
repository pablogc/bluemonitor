#!/usr/bin/python
# Simple convertion from space sepparated table to html

PARSERNAME='monit'

def parser(plain_text):
    output=''
    lines = plain_text.splitlines()
    output+='<table id="services">\n'
    idx=0
    for line in lines:
        if not line.startswith('The Monit'):
            idx+=1
            line=line.replace('Does not exist', 'Does_not_exist')
            line=line.replace('Execution failed', 'Execution_failed')
            line = line.split()
            for l in line:
                l.replace('_',  ' ')
            if idx%2 == 0:
                output+='<tr class="alt">\n<td>%s</td>\n</tr>\n' % '</td>\n<td>'.join(line)
            else:
                output+='<tr>\n<td>%s</td>\n</tr>\n' % '</td>\n<td>'.join(line)
    output+='</table>\n'
    return output
