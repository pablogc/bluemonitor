#!/usr/bin/python
import os

currdir = os.path.dirname(os.path.realpath(__file__))
PARSERS={}
for i in os.listdir(currdir):
    if not i.startswith('__') and i.endswith('.py'):
        name = i[:-3]
        print name
        m=__import__('parsers.%s' % name)
        p=getattr(m,name)
        PARSERS[p.PARSERNAME]=p.parser
        print 'loaded parser %s' % p.PARSERNAME
