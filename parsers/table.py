#!/usr/bin/python
# Simple convertion from space sepparated table to html

PARSERNAME='table'

def parser(plain_text):
    output=''
    lines = map(lambda x : x.split(), plain_text.splitlines())
    output+='<table id="services">\n'
    idx=0
    for line in lines:
        idx+=1
        if idx%2 == 0:
            output+='<tr class="alt">\n<td>%s</td>\n</tr>\n' % '</td>\n<td>'.join(line)
        else:
            output+='<tr>\n<td>%s</td>\n</tr>\n' % '</td>\n<td>'.join(line)
    output+='</table>\n'
    return output
