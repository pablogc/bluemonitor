# Purpose #

BlueMonitor is a simple tool for generating a webpage showing live information about your servers. You can configure it to connect through ssh to a list of servers and run arbitrary monitorization commands with a given refresh rate.

It uses python [BaseHttpServer](https://docs.python.org/2/library/basehttpserver.html) (in main python libraries) and [paramiko](http://www.paramiko.org/).

# Basic usage #

Edit the configuration file with your desired layout.

Execute the main script:

```
#!bash

python BlueMonitor.py

```


# Configuration example #


```
#!javascript
{
    "options": {
	"theme": "default",
	"columns": 2,
	"refresh": 5	
    },
    "servers": {
	"controller1": {
	    "ip": "192.168.244.20",
	    "password": "pass",
	    "username": "user",
	    "parser": "monit",	    
	    "command": "monit summary"
	},
	"controller2": {
	    "ip": "192.168.244.10",
	    "password": "pass",
	    "username": "user",
	    "parser": "none",	    
	    "command": "dmesg | tail"
	},
	"compute1": {
	    "ip": "192.168.244.30",
	    "password": "pass",
	    "username": "user",
	    "parser": "none",	    
	    "command": "top -n 1"
	},
	"compute2": {
	    "ip": "192.168.236.40",
	    "password": "pass",
	    "username": "user",
	    "parser": "table",	    
	    "command": "df -h"
	}
    }
}

```